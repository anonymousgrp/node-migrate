import IApiController from '@framework/interfaces/api-controller';
import ExtApiController from '@framework/decorators/extended-api-controller';
import InjectRepository from '@framework/decorators/inject-repository';
import ExtApiAction from '@framework/decorators/extended-api-action';
import Message, { MessageObject } from '@domain-objects/post/message';
import { AuthorObject } from '@domain-objects/post/author';
import IMessageRepository from '@interfaces/repositories/message-repository';
import IAuthenticationService from '@interfaces/application-services/authentication';
import TYPES from '@framework/di/types';
import { Identifier } from '@framework/ddd';
import { inject } from 'inversify';
import { ObjectID, ObjectId } from 'mongodb';
import mongoUtils from '../../utils/mongoUtils';

interface PostMessageResponse {
  success: boolean;
  data: MessageObject[];
  links: {
    self: string;
  };
}

@ExtApiController<PostsController>('posts')
export default class PostsController extends IApiController {
  @InjectRepository('MessageRepository') private messages: IMessageRepository;
  @inject(TYPES.AuthenticationService) private auth: IAuthenticationService;


/*
   API to Get all message. 
*/

  @ExtApiAction('get',
    '/',
    [],
    'messages.read',
    true)
  public async getMessagesAction(): Promise<void> {

    try {
      const token = this.request.getHeader('authorization').toString().split(' ')[1]
      const payload = await this.auth.verify(token)
    } catch (error) {
      this.Response.setStatusCode(401);
      this.Response.setBody({
        success: false,
        data: {
          message: `Not Authorized`,
        },
      });
    }

    const bodyObject: {
      data: MessageObject[];
    } = {
      /*
        Fill this array with messages, using IMessageRepository.
      */
      data: [],
    };


    bodyObject.data = await mongoUtils.getDb().collection('messages').aggregate([
      {
        $lookup: {
          from: 'users',
          localField: 'author',
          foreignField: '_id',
          as: 'author'
        }
      },
      {
        $unwind: '$author'
      },
      {
        $lookup: {
          from: 'users',
          localField: 'replies.author',
          foreignField: '_id',
          as: 'replyAuthors'
        }
      },
      {
        $sort: { created: 1 }
      },
    ]).toArray()
    this.response.setBody(bodyObject);
    this.response.setStatusCode(200);
  }

/*
   API to Post  message. 
*/


  @ExtApiAction('post',
    '/',
    [
      'message',
    ],
    'messages.write',
    true)
  public async postMessageAction(message: string): Promise<void> {
    try {
      const token = this.request.getHeader('authorization').toString().split(' ')[1]
      const payload = await this.auth.verify(token)

      const messageObj: any = {
        author: new ObjectID(payload.user._id),
        message,
        created: Date.now()
      }

      const result = await mongoUtils.getDb().collection('messages').insertOne(messageObj)
      const author = await mongoUtils.getDb().collection('users').findOne(messageObj.author)
      messageObj._id = result.insertedId
      messageObj.author = author
      console.log(messageObj);

      this.Response.setStatusCode(201);
      this.Response.setBody({
        success: true,
        data: messageObj
      });
    } catch (err) {
      this.Response.setStatusCode(500);
      this.Response.setBody({
        success: false,
        data: {
          message: `Error: ${err.message}`,
        },
      });
    }
  }

/*
   API to Delete all message  only  Admin have a  right to do it . 
*/

  @ExtApiAction('delete',
    '/',
    [],
    'messages.delete',
    true)
  public async deleteAllMessageAction(): Promise<void> {
    /*
      Make sure user is admin can delete all messages.
    */
    try {
      let user
      try {
        const token = this.request.getHeader('authorization').toString().split(' ')[1]
        const payload = await this.auth.verify(token)
        user = payload.user
      } catch (error) {
        this.response.setStatusCode(400);
        this.response.setBody({
          success: false,
          data: {
            message: error.message,
          },
        });
        return
      }

      if (user['is-admin']) {
        const result = await mongoUtils.getDb().collection('messages').deleteMany({})
        if (result) {
          this.response.setBody({
            success: true,
            data: {
              message: `Posts Deleted`,
            }
          });
        } else {
          this.response.setStatusCode(501)
          this.response.setBody({
            success: true,
            data: {
              message: `Unable to Delete All Posts`,
            }
          });
        }
      } else {
        this.response.setStatusCode(401)
        this.response.setBody({
          success: true,
          data: {
            message: `Not authorized to delete messages`,
          }
        });
      }
    } catch (error) {
      this.response.setStatusCode(400);
      this.response.setBody({
        success: false,
        data: {
          message: error.message,
        },
      });
    }
  }

 /*
   API to reply Message   
*/
  //                       Explanation of the @ExtApiAction compound decorator
  @ExtApiAction(
    //                     This is the HTTP method used for this action.
    'put',
    /*                     This is the endpoint used to reach this action.
                           It combines with the controller module and API root, in this case /posts/:id
                           The : in front of id suggests this is a variable.
    */
    '/:id',
    /*                     This array lists the names of the arguments for the action,
                           in the order they appear in the argument list.
    */
    [
      'id',
      'reply',
    ],
    /*                     This is the permissions required for this endpoint. Path variables can be used here,
                           in which case the name is substituted for the parameter.
    */
    'messages.write',
    /*                     True if endpoint requires authentication.
                           Required for permissions, since permissions are stored in JWT.
    */
    true,
  )
  public async putReplyAction(id: string, reply: string): Promise<void> {
    try {
      let payload
      try {
        const token = this.request.getHeader('authorization').toString().split(' ')[1]
        payload = await this.auth.verify(token)
      } catch (error) {
        this.response.setStatusCode(401);
        this.response.setBody({
          success: false,
          data: {
            message: `UnAutherized`,
          },
        });
        return
      }

      const replyObj = {
        _id: new ObjectID(),
        message: reply,
        author: new ObjectID(payload.user._id),
        created: Date.now()
      }

      const insertResult = await mongoUtils.getDb().collection('messages').updateOne({ _id: new ObjectID(id) }, {
        $push: { replies: replyObj }
      })
      /*
        Add reply to message with given id and update database.
      */
      this.response.setBody({
        success: true,
        data: {
          message: `Reply added to post ${id}`,
        },
        links: {
          self: `/api/posts/${id}`,
        },
      });
    } catch (error) {
      this.response.setStatusCode(400);
      this.response.setBody({
        success: false,
        data: {
          message: `Post ${id} not found.`,
        },
      });
    }
  }

/*
   API to Delete  message. 
*/
  @ExtApiAction('delete',
    '/:id',
    ['id'],
    'messages.delete',
    true)
  public async deleteMessageAction(id: string): Promise<void> {
    /*
      Make sure user is admin or owner of note, then delete message from the database.
    */
   console.log("before if")

    try {
      console.log("after try")

      const token = this.request.getHeader('authorization').toString().split(' ')[1]
      const payload = await this.auth.verify(token)
      const user = payload.user

      const result = await mongoUtils.getDb().collection('messages')
        .aggregate([
          {
            $match: {
              _id: new ObjectId(id)
            }
          },
          {
            $lookup: {
              from: 'users',
              localField: 'author',
              foreignField: '_id',
              as: 'author'
            }
          },
          {
            $unwind: '$author'
          }
        ]).toArray()

      const message = result[0]
      if (!message) {
        throw new Error("Message not found")
      }
      if (user['is-admin'] || message.author._id == user._id) {
        const result = await mongoUtils.getDb().collection('messages').deleteOne({ _id: new ObjectID(message._id) })
        if (result) {
          this.response.setBody({
            success: true,
            data: {
              message: `Post Deleted of id ${id}`,
            }
          });
        } else {
          this.response.setStatusCode(501)
          this.response.setBody({
            success: true,
            data: {
              message: `Unable to Delete Post of id ${id}`,
            }
          });
        }
      } else {
        this.response.setStatusCode(401)
        this.response.setBody({
          success: true,
          data: {
            message: `Reply added to post ${id}`,
          },
          links: {
            self: `/api/posts/${id}`,
          },
        });
      }

    } catch (error) {
      this.response.setStatusCode(400);
      this.response.setBody({
        success: false,
        data: {
          message: error.message,
        },
      });
    }
  }

/*
   API to Delete  message Reply. 
*/
  @ExtApiAction('delete',
    '/:id/:index',
    ['id', 'index'],
    'messages.delete',
    true)
  public async deleteReplyAction(id: string, index: number): Promise<void> {
    const message: Message = null;
    /*
      Make sure user is admin or owner of note, then delete message from the database.
    */
   console.log("reply actions")

    try {
      const token = this.request.getHeader('authorization').toString().split(' ')[1]
      const payload = await this.auth.verify(token)
      const user = payload.user

      const result = await mongoUtils.getDb().collection('messages').findOne({ _id: new ObjectID(id) })
      const reply = result.replies[index];
      console.log(result)
      if (reply) {

        if (reply.author.toString() === user._id || user['is-admin']) {
          console.log("flase")
          let result = await mongoUtils.getDb().collection('messages').updateOne({ _id: new ObjectID(id) }, {
            $unset: { [`replies.${index}`]: 1 }
          })
          result = await mongoUtils.getDb().collection('messages').updateOne({ _id: new ObjectID(id) }, {
            $pull: { replies: null }
          })
          this.response.setStatusCode(200)
          this.response.setBody({
            success: true,
            data: {
              message: 'Reply Deleted',
            }
          })
        } else {
          this.response.setStatusCode(401)
          this.response.setBody({
            success: false,
            data: {
              message: 'Not authorized to delete someone else reply',
            }
          })
        }
      } else {
        this.response.setStatusCode(404);
        this.response.setBody({
          success: false,
          data: {
            message: 'Reply not found',
          },
        });
      }
    } catch (error) {
      this.response.setStatusCode(400);
      this.response.setBody({
        success: false,
        data: {
          message: error.message,
        },
      });
    }
  }
}
