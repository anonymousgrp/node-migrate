import { MongoClient, Db } from 'mongodb';

let _db: Db;

const mongoUtils = {
    connect: (cb: Function) => {
        MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true }, (err, client) => {
            _db = client.db('nodeMigrate')
            return cb(err)
        })
    },
    getDb: () => _db
}

export default mongoUtils;